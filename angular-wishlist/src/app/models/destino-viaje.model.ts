export class DestinoViaje {
    private selected: boolean;
    public servicios: string[];
    public id: string;
    constructor( public nombre:string, public url:string){
        this.servicios =['picina','desayuno','tour'];
    }
    isSelected(): boolean{
        return this.selected;
    }
    setSelected(s: boolean){
        this.selected = s;
    }
}