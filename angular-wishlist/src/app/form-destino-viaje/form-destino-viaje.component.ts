import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';



@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.scss']
})
export class FormDestinoViajeComponent implements OnInit {
  
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg:FormGroup;
  minLongitud =5;
  searchResults: string[];
  
  constructor(fb: FormBuilder) { 
    this.onItemAdded = new EventEmitter
    this.fg = fb.group({
      nombre: ['',Validators.compose([
        Validators.required,
        this.nombreValidator,
        this.nameValidatorParametrizable(this.minLongitud)
      ])],
      url: ['']
    });
    
    //ver cambios en el formulario por consola
    this.fg.valueChanges.subscribe((form: any) =>{
    console.log('cambió el formulario: ', form);
    });
  }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    //escucha cada vez que se presione una tecla
    fromEvent(elemNombre, 'input')
    .pipe(
      map((e: KeyboardEvent) =>(e.target as HTMLInputElement).value),
      filter(text => text.length > 2),
      debounceTime(200),
      distinctUntilChanged(),
    

    )
  }


guardar(nombre:string, url:string): boolean{
  let d = new DestinoViaje(nombre, url);
  this.onItemAdded.emit(d);
  return false

}
//La funcion devuelve un objeto con una key -s- y un valor boolean ej {'required': true}
nombreValidator(control: FormControl):{[s: string]: boolean}{
  // trim()quita los espacios
  const longitud = control.value.toString().trim().length;
  if (longitud> 0 && longitud < 5){
    return {invalidNombre: true};
  }
  return null;
}

nameValidatorParametrizable(milog: Number):ValidatorFn {
 return (control: FormControl):{[s: string]: boolean}  =>
 {
      // trim()quita los espacios
      const longitud = control.value.toString().trim().length;
      if (longitud> 0 && longitud < milog){
        return {minLongNombre: true};
      }
    }
  }


}
